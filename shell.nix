with (import <nixpkgs> { });
let
  chatty = fetchzip {
    url =
      "https://github.com/chatty/chatty/releases/download/v0.13.1/Chatty_0.13.1.zip";
    sha256 = "16c0hp1gsg6j2b2cw7zd2dc4w921i1djdmcpk1id03as0adv0f67";
    stripRoot = false;
    name = "Chatty_0.13.1";
  };
in mkShell {
  buildInputs = [
    bash
    coreutils
    nodejs-14_x
    elmPackages.elm
    foreman
    screenkey

    nixfmt
    jdk11
  ];
  shellHook = ''
    alias chatty="${jdk11}/bin/java -jar ${chatty}/Chatty.jar"
  '';
}
