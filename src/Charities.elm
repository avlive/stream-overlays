module Charities exposing (..)

{-| A library of different charities that have been supported in the past.
-}


type alias CharityInfo =
    { name : String
    , description : String
    , donateLink : String
    }


annieCannons : CharityInfo
annieCannons =
    { name = "AnnieCannons"
    , description = "Transforming survivors of human trafficking into software professionals"
    , donateLink = "https://is.gd/jojipi"
    }


blackGirlsCode : CharityInfo
blackGirlsCode =
    { name = "Black Girls Code"
    , description = "Introducing computer coding lessons to young girls from underrepresented communities"
    , donateLink = "https://bit.ly/blackgirlsarethefuture"
    }


blackLivesMatter : CharityInfo
blackLivesMatter =
    { name = "Black Lives Matter"
    , description = ""
    , donateLink = "https://blacklivesmatter.com/"
    }


aclu : CharityInfo
aclu =
    { name = "ACLU"
    , description = "calling for immediate resignation of Kenosha police chief, sheriff"
    , donateLink = "https://action.aclu.org/give/now"
    }


code2040 : CharityInfo
code2040 =
    { name = "Code2040"
    , description = "connecting, and mobilizing the largest racial equity community in tech"
    , donateLink = "https://code2040-2020.funraise.org/"
    }


kelsey30 : CharityInfo
kelsey30 =
    { name = "Ms Kelsey's Girls Who Code Scholarship"
    , description = ""

    --"Help some deserving students afford a computer science education"
    , donateLink = "https://www.gofundme.com/f/ms-kelseys-girls-who-code"
    }


culturalConservancy : CharityInfo
culturalConservancy =
    { name = "The Cultural Conservancy"
    , description = "to protect and restore indigenous cultures"
    , donateLink = "www.nativeland.org"
    }
