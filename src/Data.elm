module Data exposing (Data, Schedule, ScheduleItem, data)

{-| The data about the stream that is used by all scenes.
-}

import Charities
import Components.Timer exposing (Timer)
import Data.Timer
import Music
import Time exposing (Weekday(..))


data : Data
data =
    { title = "3D graphics and physics [Elm]"
    , summary = Just "Elm Game Jam #5 theme: \"Three-dimensional\""
    , startAt = Time.millisToPosix 1608487236000
    , goals = []
    , schedule =
        { past =
            []
        , upcoming =
            [ ScheduleItem Nothing "Gamify the game"
            , ScheduleItem Nothing "More concrete shaders"
            , ScheduleItem Nothing "More ephemeral shaders"
            , ScheduleItem Nothing "(if time) Receive musical inspiration"
            ]
        }
    , nextStream =
        regularSchedule Tue
    , timer = Data.Timer.timer
    , charity = Just Charities.blackGirlsCode
    , music = Just Music.adhesiveWombat
    , links =
        [ "https://itch.io/jam/elm-game-jam-5"
        , "https://gitlab.com/avlive/elm-game-jam-5"
        ]
    }


regularSchedule :
    Time.Weekday
    ->
        Maybe
            { time : String
            , title : String
            }
regularSchedule day =
    case day of
        Tue ->
            Just
                { time = "Tue 2pm PST / 10pm UTC"
                , title = "Learning to Haskell"
                }

        Thu ->
            Just
                { time = "Thu 11:30am PST / 7:30pm UTC"
                , title = "Making Elm better"
                }

        Sun ->
            Just
                { time = "Sun 10am PST / 6pm UTC"
                , title = "Elm Game Jam hangout"
                }

        _ ->
            Nothing


type alias Data =
    { title : String
    , summary : Maybe String
    , startAt : Time.Posix
    , goals : List ( String, Time.Posix )
    , schedule : Schedule
    , nextStream :
        Maybe
            { time : String
            , title : String
            }
    , timer : Maybe Timer
    , charity : Maybe Charities.CharityInfo
    , music : Maybe Music.MusicInfo
    , links : List String
    }


type alias Schedule =
    { past : List ScheduleItem
    , upcoming : List ScheduleItem
    }


type alias ScheduleItem =
    { time : Maybe String
    , description : String
    }
