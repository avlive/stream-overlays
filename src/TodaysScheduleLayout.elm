module TodaysScheduleLayout exposing (Config, view)

import Animation.Extra
import Components.Countdown as Countdown
import Data exposing (Data)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (withAlpha)
import Element.Font as Font
import Html exposing (Html, a, b, br, div, h1, h3, i, li, main_, span, ul)
import Html.Attributes exposing (attribute, class, style)
import Html.Extra exposing (viewMaybe)
import Palette
import Scenes.Calendar as Calendar
import Time


type alias Config =
    { fadeBottom : Bool
    , sceneTitle : Maybe String
    , scheduleTitle : String
    , highlightFirst : Bool
    }


view : Config -> Time.Posix -> Data -> Html msg
view config now data =
    main_
        [ class "main-scene gradient"
        , attribute "role" "main"
        ]
        [ div
            [ class "positioned"
            , style "width" "320px"
            , style "height" "100%"
            ]
            [ div
                [ style "padding" "300px 40px 100px 20px"
                , style "font-size" "20px"
                ]
                [ if data.links /= [] then
                    Html.text "Today's links:"

                  else
                    Html.text ""
                , let
                    viewLink link =
                        div
                            [ style "line-height" "1"
                            , style "margin-top" "2px"
                            , style "margin-bottom" "8px"
                            ]
                            [ a [] [ Html.text <| String.replace "/" "\u{200B}/" link ] ]
                  in
                  div [] <|
                    List.map viewLink data.links
                ]
            , let
                viewCharity charity =
                    div
                        [ class "positioned"
                        , style "bottom" "0"
                        , style "height" "170px"
                        , style "font-size" "20px"
                        , style "padding" "10px"
                        ]
                        [ span [ class "dim" ] [ Html.text "Today's cause: " ]
                        , Html.text charity.name
                        , br [] []
                        , div
                            [ style "font-size" "80%"
                            , style "line-height" "1.1"
                            , style "padding" "5px"
                            ]
                            [ i [] [ Html.text charity.description ]
                            ]
                        , span [ class "dim" ] [ Html.text "Donate at " ]
                        , a [] [ Html.text charity.donateLink ]
                        ]
              in
              viewMaybe viewCharity data.charity
            ]
        , div
            [ class <|
                if config.fadeBottom then
                    "positioned fade-bottom"

                else
                    "positioned"
            , style "left" "320px"
            ]
            [ h1
                [ class "project-name" ]
                [ Html.text data.title ]
            , case data.summary of
                Nothing ->
                    Html.text ""

                Just summary ->
                    Html.p
                        [ style "font-size" "24px" ]
                        [ Html.text summary ]
            , [ row
                    [ width fill
                    , height (px 70)
                    ]
                <|
                    case config.sceneTitle of
                        Just title ->
                            [ el
                                [ Palette.fontSize.h2
                                , Font.bold
                                ]
                                (text title)
                            , el
                                [ centerX
                                , Palette.fontSize.h2
                                ]
                                (Countdown.view now data.startAt)
                            ]

                        Nothing ->
                            []
              ]
                |> column
                    [ width fill
                    , paddingEach { top = 45, bottom = 25, left = 0, right = 20 }
                    ]
                |> layout [ Font.color Palette.color.mainText ]
            , h3 [] [ Html.text config.scheduleTitle ]
            , let
                viewItem item =
                    li []
                        [ b [ class "schedule" ]
                            [ Html.text (Maybe.withDefault "❧" item.time) ]
                        , Html.text item.description
                        ]
              in
              ul
                [ class <|
                    if config.highlightFirst then
                        "large-schedule highlight-first"

                    else
                        "large-schedule"
                ]
              <|
                List.map viewItem data.schedule.upcoming
            , br [] []
            ]
        , let
            viewMusic info =
                div
                    [ class "positioned"
                    , style "right" "15px"
                    , style "bottom" "10px"
                    , style "font-size" "16px"
                    ]
                    [ span [ class "dim" ] [ Html.text "Music by " ]
                    , Html.text info.name
                    , Html.text " "
                    , a [] [ Html.text info.link ]
                    ]
          in
          viewMaybe viewMusic data.music
        , if config.sceneTitle == Nothing then
            -- show the countdown here
            el
                [ alignRight
                , alignBottom
                , htmlAttribute (style "zoom" "2")
                ]
                (Countdown.view now data.startAt)
                |> layout [ padding 50 ]

          else
            Html.text ""
        , let
            opacity =
                Animation.Extra.toggle ( 40, "0" ) ( 20, "1" ) now
          in
          layout
            [ width fill
            , height fill
            , padding 50
            , Background.color (Palette.color.grayBackground |> withAlpha 0.6)
            , htmlAttribute (Html.Attributes.style "transition" "all 1s")
            , htmlAttribute (Html.Attributes.style "opacity" opacity)
            , inFront
                (el
                    [ Font.color Palette.color.mainText
                    , Font.size 22
                    , padding 10
                    , Font.extraBold
                    , Font.letterSpacing 1.5
                    ]
                    (text "Upcoming Schedule")
                )
            ]
          <|
            el
                [ Background.color Palette.color.grayBackground
                , width fill
                , height fill
                , Border.rounded 20
                , Border.shadow
                    { offset = ( 1, 3 )
                    , size = 4
                    , blur = 8
                    , color =
                        Palette.color.grayBackground
                            |> withAlpha 0.6
                    }
                , clip
                ]
                (el
                    [ alignLeft
                    , height fill
                    ]
                    (Calendar.view now)
                )
        ]
