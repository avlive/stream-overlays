module Scenes.Calendar exposing (view)

import DateFormat
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (withAlpha)
import Element.Font as Font
import Html
import Html.Attributes
import Palette
import Time
import Time.Extra
import TimeZone


hourAtTop =
    9.5


tz =
    TimeZone.america__los_angeles ()


widths =
    let
        dayWidth =
            230
    in
    { day = dayWidth
    , divider = 50
    , infoColumn = 4 * dayWidth // 5
    }


data : List Day
data =
    [ Day
        { day = ( 2020, Time.Dec, 1 )
        , events =
            [ MyEvent
                { time = "2pm PST"
                , time2 = " / 10pm UTC"
                , startHour = 14
                , duration = 2
                , details =
                    [ { time = Nothing
                      , title = "Learning to Haskell"
                      , description = ""
                      }
                    ]
                }
            ]
        }
    , Day
        { day = ( 2020, Time.Dec, 2 )
        , events =
            []
        }
    , Day
        { day = ( 2020, Time.Dec, 3 )
        , events =
            [ MyEvent
                { time = "11:30pm PST"
                , time2 = " / 7:30pm UTC"
                , startHour = 11.5
                , duration = 2.5
                , details =
                    [ { time = Nothing
                      , title = "Making Elm better"
                      , description = ""
                      }
                    ]
                }
            ]
        }
    , Day
        { day = ( 2020, Time.Dec, 4 )
        , events =
            []
        }
    , Day
        { day = ( 2020, Time.Dec, 5 )
        , events = []
        }
    , Day
        { day = ( 2020, Time.Dec, 6 )
        , events =
            [ MyEvent
                { time = "10am PST"
                , time2 = " / 6pm UTC"
                , startHour = 10
                , duration = 2.5
                , details =
                    [ { time = Nothing
                      , title = "Elm Game Jam hangout"
                      , description = ""
                      }
                    ]
                }
            ]
        }
    ]


view : Time.Posix -> Element msg
view now =
    if True then
        let
            widthOf day =
                case day of
                    Day _ ->
                        widths.day

                    Divider _ ->
                        widths.divider

                    InfoColumn _ ->
                        widths.infoColumn

            totalWidth =
                List.map widthOf data
                    |> List.sum

            totalHeight =
                totalWidth // 2
        in
        row
            [ Background.color Palette.color.grayBackground
            , width (px (totalHeight * 2))
            , height (px totalHeight)
            ]
            (List.map (viewDay now) data)

    else
        html (Html.textarea [ Html.Attributes.style "height" "400px" ] [ Html.text asString ])


asString : String
asString =
    let
        renderDay d =
            case d of
                Day day ->
                    List.concatMap (renderEvent day) day.events

                Divider info ->
                    [ info.text ++ ":" ]

                InfoColumn info ->
                    [ "- " ++ String.concat (List.map Tuple.second info.text) ]

        renderEvent day e =
            case e of
                MyEvent event ->
                    case event.details of
                        [ deet ] ->
                            [ "- " ++ dayString day.day ++ ", " ++ event.time ++ event.time2 ++ ": " ++ deet.title ++ " — " ++ deet.description ]

                        _ ->
                            List.concat
                                [ [ "- " ++ dayString day.day ++ ", " ++ event.time ++ event.time2 ++ ":" ]
                                , List.map (\deet -> "  - " ++ deet.title ++ " — " ++ deet.description) event.details
                                ]

                OtherEvent event ->
                    [ "- " ++ dayString day.day ++ ", " ++ event.time ++ ": (elsewhere) " ++ event.text ]
    in
    "Upcoming stream schedule:\n"
        ++ (data
                |> List.concatMap renderDay
                |> String.join "\n"
           )


type Day
    = Day DayInfo
    | Divider
        { text : String
        }
    | InfoColumn
        { text : List ( List (Attribute Never), String )
        }


type alias DayInfo =
    { day : ( Int, Time.Month, Int )
    , events : List Event
    }


type Event
    = MyEvent
        { time : String
        , time2 : String
        , startHour : Float
        , duration : Float
        , details :
            List
                { title : String
                , description : String
                , time :
                    Maybe
                        { time : String
                        , time2 : String
                        }
                }
        }
    | OtherEvent
        { time : String
        , startHour : Float
        , duration : Float
        , text : String
        }


viewDay : Time.Posix -> Day -> Element msg
viewDay now d =
    case d of
        Day day ->
            column
                [ height fill
                , Border.color (rgb 0.5 0.5 0.5)
                , Border.widthXY 1 0
                , Border.dotted
                , padding 10
                ]
                [ el [ height (px 20) ] none
                , el
                    [ Font.color Palette.color.mainText
                    , Font.extraBold
                    , alpha 0.8
                    , Font.size 21
                    ]
                    (text (dayString day.day))
                , viewEvents now day
                ]

        Divider info ->
            el
                [ Border.widthXY 2 0
                , Border.color (Palette.color.mainText |> withAlpha 0.6)
                , height fill
                , Background.color (Palette.color.mainText |> withAlpha 0.2)
                , alignTop
                , paddingXY 5 20
                , width (px widths.divider)
                ]
                (el
                    [ rotate (degrees -90)
                    , centerX
                    , moveDown 180
                    , Font.color Palette.color.mainText
                    , Font.bold
                    , Font.size 30
                    ]
                    (text ("⌄⌄  " ++ info.text ++ "  ⌄⌄"))
                )

        InfoColumn info ->
            column
                [ width (px widths.infoColumn)
                , height fill
                , Border.color (rgb 0.5 0.5 0.5)
                , Border.widthXY 1 0
                , Border.dotted
                , Background.color (Palette.color.mainText |> withAlpha 0.05)
                , padding 10
                , Font.italic
                ]
                [ el [ height (px 80) ] none
                , paragraph
                    [ Font.color (Palette.color.mainText |> withAlpha 0.8)
                    , Font.size 20
                    , width fill
                    , padding 10
                    ]
                    (List.map (\( attrs, text ) -> el attrs (Element.text text)) info.text)
                    |> Element.map never
                ]


dayString ( year, month, date ) =
    let
        dayPosix =
            Time.Extra.partsToPosix
                tz
                { year = year
                , month = month
                , day = date
                , hour = 12
                , minute = 0
                , second = 0
                , millisecond = 0
                }
    in
    DateFormat.format
        [ DateFormat.dayOfWeekNameFull
        ]
        tz
        dayPosix


hourSize =
    84


viewEvents : Time.Posix -> DayInfo -> Element msg
viewEvents now day =
    case List.map (viewEvent now day) day.events of
        [] ->
            el
                [ Font.color Palette.color.mainText
                , alpha 0.4
                , Font.size 20
                , height (px <| hourSize * 3)
                , width (px <| widths.day // 2)
                ]
                (paragraph
                    [ centerX
                    , centerY
                    , Font.center
                    ]
                    [ text "No stream today" ]
                )

        first :: rest ->
            el [ width (px widths.day) ] <|
                List.foldl
                    (\next stack ->
                        el
                            [ width fill
                            , behindContent stack
                            ]
                            next
                    )
                    first
                    rest


viewEvent : Time.Posix -> DayInfo -> Event -> Element msg
viewEvent now day e =
    let
        ( startHour, endHour ) =
            case e of
                MyEvent record ->
                    ( record.startHour, record.startHour + record.duration )

                OtherEvent record ->
                    ( record.startHour, record.startHour + record.duration )

        moveDownForStart =
            moveDown (hourSize * (startHour - hourAtTop))

        heightForDuration duration =
            height (shrink |> minimum (round (hourSize * duration) - 10))

        isToday =
            day.day
                == ( Time.toYear tz now
                   , Time.toMonth tz now
                   , Time.toDay tz now
                   )

        ( year, month, date ) =
            day.day

        timestampOfEnd =
            Time.Extra.partsToPosix tz
                { year = year
                , month = month
                , day = date
                , hour = endHour |> floor
                , minute = endHour * 60 |> round |> modBy 60
                , second = 0
                , millisecond = 0
                }

        isPast =
            --Time.posixToMillis timestampOfEnd < Time.posixToMillis now
            False

        dimIfPast =
            case isPast of
                True ->
                    0.6

                False ->
                    1

        viewTime event =
            row
                [ Font.size 13
                ]
                [ el
                    [ Font.extraBold
                    , Font.color theme.startTimeColor
                    ]
                    (text event.time)
                , text event.time2
                ]

        theme =
            if isToday then
                { borderColor = Palette.color.accentBold
                , startTimeColor = Palette.color.accentBold
                , gradientAngle = degrees 140
                }

            else
                { borderColor = rgb 0.6 0.7 0.67
                , startTimeColor = Palette.color.accent
                , gradientAngle = degrees 140
                }
    in
    case e of
        MyEvent event ->
            el
                [ Background.gradient
                    { angle = theme.gradientAngle
                    , steps =
                        [ Palette.color.bgGradientBlue
                        , Palette.color.bgGradientGreen
                        ]
                    }
                , Font.color Palette.color.mainText
                , width fill
                , Border.rounded 11
                , Border.color theme.borderColor
                , Border.width 3
                , padding 10
                , moveDownForStart
                , heightForDuration event.duration
                , alpha dimIfPast
                ]
                (column
                    [ spacing 7
                    ]
                    [ viewTime event
                    , let
                        viewDeet deet =
                            column
                                [ spacing 7
                                ]
                                [ Maybe.map viewTime deet.time
                                    |> Maybe.withDefault none
                                , paragraph
                                    [ spacing 2
                                    , Font.size <|
                                        24
                                    ]
                                    [ text deet.title
                                    ]
                                , case deet.description of
                                    "" ->
                                        none

                                    _ ->
                                        paragraph
                                            [ spacing 1
                                            , Font.size 16
                                            , alpha 0.8
                                            ]
                                            [ text deet.description
                                            ]
                                ]
                      in
                      column [ spacing 21 ]
                        (List.map viewDeet event.details)
                    ]
                )

        OtherEvent event ->
            el
                [ Font.color Palette.color.mainText
                , width fill
                , Border.rounded 11
                , Border.color theme.borderColor
                , Border.width 2
                , padding 10
                , moveDownForStart
                , heightForDuration event.duration
                , alpha (0.8 * dimIfPast)
                ]
                (column
                    [ spacing 5
                    ]
                    [ el
                        [ Font.size 13
                        , Font.bold
                        ]
                        (text event.time)
                    , paragraph
                        [ spacing 2
                        , Font.size 15
                        , alpha 0.8
                        ]
                        [ text event.text
                        ]
                    ]
                )
