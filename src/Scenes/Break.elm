module Scenes.Break exposing (view)

import Data exposing (Data)
import Html exposing (..)
import Time
import TodaysScheduleLayout


view : Time.Posix -> Data -> Html msg
view =
    TodaysScheduleLayout.view
        { fadeBottom = True
        , sceneTitle = Just "On a short break"
        , scheduleTitle = "Up next:"
        , highlightFirst = True
        }
