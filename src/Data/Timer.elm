module Data.Timer exposing (timer)

import Components.Timer as Timer
import Time


lastTime h m s ms =
    ((((h * 60) + m) * 60 + s) * 1000) + ms


timer =
    -- TDD project
    --Just ( lastTime 31 11 55 0, Nothing )
    -- elm-animator project
    --Just ( lastTime 3 34 20 0, Nothing )
    --    |> Timer.startTimer (Time.millisToPosix 1597165935000)
    Nothing
