module Element.Extra exposing (withAlpha)

import Element exposing (Color, rgba, toRgb)


withAlpha : Float -> Color -> Color
withAlpha newAlpha color =
    let
        c =
            toRgb color
    in
    rgba c.red c.green c.blue (newAlpha * c.alpha)
