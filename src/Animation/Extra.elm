module Animation.Extra exposing (toggle)

import Time


toggle : ( Int, a ) -> ( Int, a ) -> Time.Posix -> a
toggle ( aHoldTime, aValue ) ( bHoldTime, bValue ) now =
    let
        t =
            Time.posixToMillis now
                |> modBy ((aHoldTime + bHoldTime) * 1000)
    in
    if t <= aHoldTime * 1000 then
        aValue

    else
        bValue
