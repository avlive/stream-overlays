#!/bin/bash

set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
DATE_CMD="$(which gdate || which date)"
stopTime="Time.millisToPosix $($DATE_CMD +%s)000"

echo "        |> Timer.stopTimer ($stopTime)" >> "$SCRIPTPATH/src/Data/Timer.elm"
