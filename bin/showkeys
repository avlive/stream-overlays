#!/bin/bash

set -euo pipefail

XRANDR_CMD="$(which xrandr)"
SCREENKEY_CMD="$(which screenkey)"
MONITOR_NAME="${1:-}"

if [[ -z "$MONITOR_NAME" ]]; then
	echo "$0: must specify a monitor name" >&2
	"$XRANDR_CMD" --listmonitors >&2
	exit 1
fi

MONITOR_INDEX="$("$XRANDR_CMD" --listmonitors | grep "${MONITOR_NAME}$" | head -n1 | sed -e 's%^ *\([0-9]*\): *[^ ]* *[0-9]*/[0-9]*x[0-9]*/[0-9]*+[0-9]*+[0-9]* .*$%\1%')"
MONITOR_X="$("$XRANDR_CMD" --listmonitors | grep "${MONITOR_NAME}$" | head -n1 | sed -e 's%^ *[0-9]*: *[^ ]* *[0-9]*/[0-9]*x[0-9]*/[0-9]*+\([0-9]*\)+[0-9]* .*$%\1%')"
MONITOR_Y="$("$XRANDR_CMD" --listmonitors | grep "${MONITOR_NAME}$" | head -n1 | sed -e 's%^ *[0-9]*: *[^ ]* *[0-9]*/[0-9]*x[0-9]*/[0-9]*+[0-9]*+\([0-9]*\) .*$%\1%')"
MONITOR_W="$("$XRANDR_CMD" --listmonitors | grep "${MONITOR_NAME}$" | head -n1 | sed -e 's%^ *[0-9]*: *[^ ]* *\([0-9]*\)/[0-9]*x[0-9]*/[0-9]*+[0-9]*+[0-9]* .*$%\1%')"
MONITOR_H="$("$XRANDR_CMD" --listmonitors | grep "${MONITOR_NAME}$" | head -n1 | sed -e 's%^ *[0-9]*: *[^ ]* *[0-9]*/[0-9]*x\([0-9]*\)/[0-9]*+[0-9]*+[0-9]* .*$%\1%')"

OVERLAY_H=100
OVERLAY_W=$(($MONITOR_W * 2 / 5))
OVERLAY_X=$(($MONITOR_X + $MONITOR_W - $OVERLAY_W))
OVERLAY_Y=$(($MONITOR_Y + $MONITOR_H - $OVERLAY_H - 50))


set | grep MONITOR
set | grep OVERLAY

"$SCREENKEY_CMD" \
	--mods-only --vis-shift \
	--no-systray \
	--scr "$MONITOR_INDEX" \
	--position fixed \
	--geometry="${OVERLAY_W}x${OVERLAY_H}+${OVERLAY_X}+${OVERLAY_Y}"
