#!/bin/bash

set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
DATE_CMD="$(which gdate || which date)"
newStart="Time.millisToPosix $($DATE_CMD +%s)000"

echo "        |> Timer.startTimer ($newStart)" >> "$SCRIPTPATH/src/Data/Timer.elm"
