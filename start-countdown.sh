#!/bin/bash

set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
DATE_CMD="$(which gdate || which date)"
date1=$(($($DATE_CMD +%s) + $1 * 60));

sed --in-place= -e "s/startAt = .*/startAt = Time.millisToPosix ${date1}000/" "$SCRIPTPATH/src/Data.elm"
